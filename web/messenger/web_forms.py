#coding=utf8

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from models import TamoUser

class RegistrationForm(forms.Form):
    username = forms.CharField(label='Vartotojo vardas', required=True, max_length=30,
                               widget=forms.TextInput(attrs={'class': 'input-block',
                                                             'placeholder': 'Vartotojo vardas'}))
    password = forms.CharField(label=u'Slaptažodis', required=True,
                               widget=forms.PasswordInput(attrs={'class': 'input-block',
                                                                 'placeholder': u'Slaptažodis'}))
    repeat_password = forms.CharField(label=u'Slaptažodžio pakartojimas',
                                      widget=forms.PasswordInput(attrs={'class': 'input-block',
                                                                         'placeholder': u'Slaptažodžio pakartojimas'}))
    email = forms.EmailField(label=u'El. paštas', required=True, max_length=75,
                             widget=forms.TextInput(attrs={'class': 'input-block',
                                                            'placeholder': u'El. paštas'}))

    def clean_username(self):
        username = self.cleaned_data.get('username')

        try:
            User.objects.get(username__exact=username)
            raise forms.ValidationError('Toks vartotojo vardas jau naudojamas')
        except User.DoesNotExist:
            return username

    def clean_email(self):
        email = self.cleaned_data.get('email')

        try:
            User.objects.get(email__exact=email)
            raise forms.ValidationError(u'Toks el. pašto adresas jau naudojamas')
        except User.DoesNotExist:
            return email

    def clean(self):
        super(RegistrationForm, self).clean()

        password = self.cleaned_data.get('password')
        repeat_password = self.cleaned_data.get('repeat_password')

        if password != repeat_password:
            raise forms.ValidationError(u'Slaptažodžiai nesutampa')

        return self.cleaned_data

class LoginForm(forms.Form):
    username = forms.CharField(label='Vartotojo vardas', required=True, max_length=30,
                               widget=forms.TextInput(attrs={'class': 'input-block',
                                                             'placeholder': 'Vartotojo vardas'}))
    password = forms.CharField(label=u'Slaptažodis', required=True,
                               widget=forms.PasswordInput(attrs={'class': 'input-block',
                                                                 'placeholder': u'Slaptažodis'}))

    def clean(self):
        super(LoginForm, self).clean()

        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = authenticate(username=username, password=password)

            if user is not None:
                if not user.is_active:
                    raise forms.ValidationError('Vartotojas yra neaktyvus')
            else:
                raise forms.ValidationError('Neteisingi prisijungimo duomenys')

        return self.cleaned_data

class TamoAccountForm(forms.ModelForm):
    class Meta(object):
        model = TamoUser
        fields = ['username', 'password', 'phone']
        widgets = {
            'username': forms.TextInput(attrs={'class': 'input-block',
                                               'placeholder': 'Vartotojo vardas'}),
            'password': forms.PasswordInput(attrs={'class': 'input-block',
                                               'placeholder': u'Slaptažodis'}),
            'phone': forms.TextInput(attrs={'class': 'input-block',
                                            'placeholder': 'Telefono numeris (+370)'})
        }

    def clean_phone(self):
        phone = self.cleaned_data.get('phone')

        if len(phone) == 12 and phone[:4] == '+370' and phone[4:].isdigit():
            return phone
        else:
            raise forms.ValidationError('Blogas telefono numerio formatas')

class FunctionsForm(forms.ModelForm):
    class Meta(object):
        model = TamoUser
        fields = ['send_grades', 'send_tests']