#coding=utf8

from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth import authenticate, login as login_user
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from web_forms import RegistrationForm, LoginForm

def registration(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            email = form.cleaned_data.get('email')

            #Create new user
            User.objects.create_user(username, email, password)

            messages.add_message(request, messages.SUCCESS, u'Vartotojas buvo sėkmingai sukurtas.\
             <a href="%s">Prisijungti</a>' % reverse('messenger:index'))
    else:
        form = RegistrationForm()

    return render(request, 'messenger/registration.html', {'title': 'Registracija',
                                                           'form': form})

def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('messenger:user:index'))

    if request.method == 'POST':
        form = LoginForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')

            user = authenticate(username=username, password=password)
            login_user(request, user)

            return HttpResponseRedirect(reverse('messenger:user:index'))
    else:
        form = LoginForm()

    return render(request, 'messenger/login.html', {'title': 'Prisijungimas',
                                                    'form': form})