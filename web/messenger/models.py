#coding=utf8

from django.db import models
from django.contrib.auth.models import User

class TamoUser(models.Model):
    user = models.ForeignKey(User, primary_key=True)
    username = models.CharField('Vartotojo vardas', max_length=30, unique=True, blank=False,
                                error_messages={'unique': 'Toks TAMO vartotojas jau naudojamas'})
    password = models.CharField(u'Slaptažodis', max_length=254, blank=False)
    phone = models.CharField('Telefono numeris', max_length=12, unique=True, blank=False,
                             error_messages={'unique': 'Toks telefono numeris jau naudojamas'})
    send_grades = models.BooleanField(u'Pažymiai', default=True)
    send_tests = models.BooleanField('Atsiskaitomieji darbai', default=False)