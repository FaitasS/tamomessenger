from django.conf.urls import patterns, url, include

user_urlpatterns = patterns('messenger.user',
    url(r'^$', 'index', name='index'),
    url(r'^naujas-tamo-vartotojas/$', 'add_tamo', name='add_tamo'),
    url(r'^tamo-vartotojas/$', 'tamo', name='tamo'),
    url(r'^trinti-tamo-vartotoja/$', 'delete_tamo', name='delete_tamo'),
    url(r'^funkcijos/$', 'functions', name='functions'),
    url(r'^atsijungti/$', 'logout', name='logout')
)

urlpatterns = patterns('messenger.views',
    url(r'^$', 'login', name='index'),
    url(r'^registracija/$', 'registration', name='registration'),
    url(r'^vartotojas/', include(user_urlpatterns, namespace='user'))
)