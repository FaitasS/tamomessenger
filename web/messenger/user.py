#coding=utf8

from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as logout_user
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from models import TamoUser
from django.contrib import messages
from django.shortcuts import render
from web_forms import TamoAccountForm, FunctionsForm

@login_required(login_url='/')
def index(request):
    return render(request, 'messenger/user/index.html', {'title': 'Pagrindinis'})

@login_required(login_url='/')
def add_tamo(request):
    try:
        TamoUser.objects.get(pk=request.user.id)
        return HttpResponseRedirect(reverse('messenger:user:tamo'))
    except TamoUser.DoesNotExist:
        pass

    if request.method == 'POST':
        form = TamoAccountForm(request.POST)

        if form.is_valid():
            new_tamo_user = form.save(commit=False)
            new_tamo_user.user_id = request.user.id
            new_tamo_user.save()

            messages.add_message(request, messages.SUCCESS, u'TAMO vartotojas sėkmingai pridėtas')

            return HttpResponseRedirect(reverse('messenger:user:tamo'))
    else:
        form = TamoAccountForm()

    return render(request, 'messenger/user/tamo_account.html', {'title': 'Naujas vartotojas',
                                                                'form': form,
                                                                'action': 'add'})

@login_required(login_url='/')
def tamo(request):
    try:
        tamo_user = TamoUser.objects.get(pk=request.user.id)
    except TamoUser.DoesNotExist:
        return HttpResponseRedirect(reverse('messenger:user:add_tamo'))

    if request.method == 'POST':
        form = TamoAccountForm(request.POST, instance=tamo_user)

        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, u'TAMO vartotojas sėkmingai pakeistas')
    else:
        form = TamoAccountForm(instance=tamo_user)

    return render(request, 'messenger/user/tamo_account.html', {'title': 'Vartotojo redagavimas',
                                                                'form': form,
                                                                'action': 'edit'})

@login_required(login_url='/')
def delete_tamo(request):
    try:
        TamoUser.objects.get(pk=request.user.id).delete()
        messages.add_message(request, messages.SUCCESS, u'TAMO vartotojas sėkmingai ištrintas')
    except TamoUser.DoesNotExist:
        messages.add_message(request, messages.ERROR, u'Turite pirmiausia pridėti TAMO vartotoją')

    return HttpResponseRedirect(reverse('messenger:user:add_tamo'))

@login_required(login_url='/')
def functions(request):
    try:
        tamo_user = TamoUser.objects.get(pk=request.user.id)
    except TamoUser.DoesNotExist:
        messages.add_message(request, messages.ERROR, u'Turite pirmiausia pridėti TAMO vartotoją')
        return HttpResponseRedirect(reverse('messenger:user:add_tamo'))

    if request.method == 'POST':
        form = FunctionsForm(request.POST, instance=tamo_user)

        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, u'Funkcijos sėkmingai atnaujintos')
    else:
        form = FunctionsForm(instance=tamo_user)

    return render(request, 'messenger/user/functions.html', {'title': 'Funkcijos',
                                                             'form': form})

def logout(request):
    logout_user(request)
    return HttpResponseRedirect(reverse('messenger:index'))